import "./App.css"
import React from "react" //useEffect

import AppRoute from "./routes/AppRoute"

import Header from "./components/Header"
import NewModal from "./components/NewModal"

const App = () => {
    return (
        <div className="container">
            <Header />
            <AppRoute />
            <NewModal />
        </div>
    )
}

export default App
