import React from "react"

import FavoritesTable from "../../components/FavoritesTable"

const PageFavorites = () => {
    return (
        <FavoritesTable />
    )
}

export default PageFavorites
