import types from './types'

const setModalContent = (content) =>
({
    type: types.SET_MODAL_CONTENT,
    payload: content
})

const defForExport = {setModalContent}

export default defForExport