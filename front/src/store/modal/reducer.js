import types from './types'

const reducer = (state=null, action) =>
{
    switch (action.type)
    {
        case types.SET_MODAL_CONTENT : {
            return action.payload
        }
        default:
            return state
    }
}

export default reducer