import {combineReducers} from 'redux'

import modal from './modal'
import products from './products'
import cart from './cart'
import user from './user'

const reducers = combineReducers({
    modal,
    products,
    cart,
    user
})

export default reducers