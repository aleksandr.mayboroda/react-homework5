import types from "./types"

const setUser = (user) => ({
    type: types.SET_USER,
    payload: user,
})

const setError = (error) => ({
    type: types.SET_ERROR,
    payload: error,
})

const setOrder = (order) => ({
    type: types.SET_ORDER,
    payload: order,
})

const defForExport = { setUser, setError, setOrder }

export default defForExport
