import actions from "./actions"

const { setUser, setError, setOrder } = actions

const defForExport = { setUser, setError, setOrder }

export default defForExport
