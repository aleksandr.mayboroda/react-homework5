import types from "./types"

const initialState = {
    data: JSON.parse(localStorage.getItem("user")) || {},
    orders: JSON.parse(localStorage.getItem("orders")) || [],
    error: null,
}

const reducer = (state = initialState, action) => {
    switch (action.type) {
        case types.SET_USER: {
            const user = action.payload
            if (user === null) {
                localStorage.removeItem("user")
            } else {
                localStorage.setItem("user", JSON.stringify(user))
            }
            return { ...state, data: user, error: null }
        }
        case types.SET_ERROR: {
            return { ...state, error: action.payload }
        }
        case types.SET_ORDER: {
            const orders = [...state.orders, action.payload]
            localStorage.setItem("orders", JSON.stringify(orders))
            return { ...state, orders: orders, error: null }
        }
        default:
            return state
    }
}

export default reducer
