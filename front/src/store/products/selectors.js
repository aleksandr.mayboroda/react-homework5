const getProducts = () => (state) => state.products.data
const getError = () => (state) => state.products.error
const getFavorites = () => (state) => state.products.favorites

//is in favorites... here...
const isProductInFavorites = (id) => (state) =>
    state.products.favorites.includes(id)

const countProductsInFavorites = () => (state) =>
    state.products.favorites.length > 0 ? state.products.favorites.length : 0

const defForExport = {
    getProducts,
    getError,
    getFavorites,
    isProductInFavorites,
    countProductsInFavorites,
}

export default defForExport
