import actions from "./actions"

const { toggleCart, clearCart } = actions

const defForExport = { toggleCart, clearCart }

export default defForExport
