const TOGGLE_CART = "homework/cart/TOGGLE_CART"
const CLEAR_CART = "homework/cart/CLEAR_CART"

const defForExport = { TOGGLE_CART, CLEAR_CART }

export default defForExport
