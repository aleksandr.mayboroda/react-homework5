import './style.scss'
import PropTypes from 'prop-types'
import Icon from "../Icon";

const Favorite = ({ filled = false, func }) =>
{
  return (
    <div className="favorite" onClick = {() => func()}>
        <span className="favorite__text">{filled ? 'In favorites' : 'Add to favorites'}</span>
        <Icon type="star" filled={filled}/>
    </div>
  );
}

Favorite.propTypes = {
  color: PropTypes.string,
  filled: PropTypes.bool,
  func: PropTypes.func.isRequired,
}

Favorite.defaultProps = {
  color: "#ffc107",
  filled: false,
}

export default Favorite;
