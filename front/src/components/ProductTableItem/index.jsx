import React from "react"
import PropTypes from "prop-types"

import useProductCartConfirm from '../../hooks/useProductCartConfirm'

function ProductTableItem({ product }) {
    const { name, imagePath, price, quantity } = product

    //мой хук
    const [modalHandler] = useProductCartConfirm(product)

    return (
        <tr>
            <td>
                <img width="200" src={imagePath} alt={name} />
            </td>
            <td>{name}</td>
            <td>{price}$</td>
            <td>{quantity}</td>
            <td>{price * quantity}$</td>
            <td>
                <button
                    className="product-delete-btn"
                    title="delete from cart"
                    onClick={() => modalHandler({...product, isInCart: true})}
                >
                    x
                </button>
            </td>
        </tr>
    )
}

ProductTableItem.propTypes = {
    product: PropTypes.shape({
        name: PropTypes.string.isRequired,
        articul: PropTypes.string.isRequired,
        imagePath: PropTypes.string.isRequired,
        price: PropTypes.number,
        quantity: PropTypes.number.isRequired,
    }).isRequired,
}

export default ProductTableItem
