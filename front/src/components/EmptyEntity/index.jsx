import React from "react"
import PropTypes from "prop-types"

import "./style.scss"

function EmptyEntity({ children }) {
    return <div className="entity-empty">{children}</div>
}

EmptyEntity.propTypes = {
    children: PropTypes.oneOfType([
        PropTypes.arrayOf(PropTypes.node),
        PropTypes.node
    ]).isRequired
}

export default EmptyEntity
