import React from "react"

function FormField({ field, form, ...rest }) {
    // function FormField(props) {
    // console.log('props',props)
    const { name } = field
    const { touched, errors } = form
    // console.log("field",field, name,touched, errors,rest )
    return (
        <div className={`form__element ${touched[name] && errors[name] && ('form__element-error')}`}>
            <label htmlFor={name}>{rest.label} </label>
            {rest.tag === 'textarea' ?  <textarea {...rest} {...field} id={name}/>  :  <input {...rest} {...field} id={name}/>}
           
            {touched[name] && errors[name] && <p className="form__error">{errors[name]}</p>}
        </div>
    )
}

export default FormField
