import React from 'react'
import {NavLink} from 'react-router-dom'

import './style.scss'

function Sidebar() {
    return (
        <ul className = 'menu-list'>
            <li className="menu-list__item"><NavLink exact className="menu-list__link" activeClassName="menu-list--active" to="/">Main Page</NavLink></li>
            <li className="menu-list__item"><NavLink exact className="menu-list__link" activeClassName="menu-list--active" to="/products">Products</NavLink></li>
            <li className="menu-list__item"><NavLink className="menu-list__link" activeClassName="menu-list--active" to="/cart">Cart</NavLink></li>
            <li className="menu-list__item"><NavLink className="menu-list__link" activeClassName="menu-list--active" to="/favorites">Favorites</NavLink></li>
        </ul>
    )
}

export default Sidebar
