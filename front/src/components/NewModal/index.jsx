import React from "react"
// import PropTypes from "prop-types"

import { modalSelectors, modalOperations } from "../../store/modal"
import { useDispatch, useSelector } from "react-redux"

import "./style.scss"

const NewModal = () => {

    const dispatch = useDispatch()
    const setModalContent = (content) =>
        dispatch(modalOperations.setModalContent(content))

    const modalContent = useSelector(modalSelectors.getModalContent())
    const isOpened = !!modalContent

    const {
        isCloseButton = true, //открытие окна
        background = false, //цвет шкурки
        classes = null, //классы для шкурки
        title = null, //элементы в хедер
        footer = null, //элементы в футер
        main = <h2>SMTH Here</h2>, //элементы в тело
    } = { ...modalContent }

    return (
        <div
            className={`modal ${isOpened && "active"}`}
            onClick={() => setModalContent(null)}
        >
            <div
                style={{ backgroundColor: background ? background : "" }}
                className={`modal__window ${isOpened && "active"} ${
                    classes && classes
                }`}
                onClick={(e) => e.stopPropagation()}
            >
                {isCloseButton && (
                    <span
                        className="modal__close"
                        onClick={() => setModalContent(null)}
                    ></span>
                )}
                {title && <div className="modal__header">{title}</div>}
                {main && <div className="modal__main">{main}</div>}
                {footer && <div className="modal__footer">{footer}</div>}
            </div>
        </div>
    )
}

// NewModal.protoTypes = {
//     isOpened: PropTypes.bool.isRequired,
//     setModalContent: PropTypes.func.isRequired,
//     closeButton: PropTypes.bool,
//     rest: PropTypes.oneOfType([
//         PropTypes.oneOf([null]),
//         PropTypes.shape({
//             isCloseButton: PropTypes.bool,
//             className: PropTypes.string,
//             background: PropTypes.string,
//             classes: PropTypes.string,
//             title: PropTypes.oneOfType([PropTypes.string, PropTypes.element]),
//             main: PropTypes.oneOfType([
//                 PropTypes.string.isRequired,
//                 PropTypes.element.isRequired,
//             ]),
//             footer: PropTypes.oneOfType([PropTypes.string, PropTypes.element]),
//         }),
//     ]),
// }

export default NewModal
