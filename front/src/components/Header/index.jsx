import React from "react"

import "./style.scss"

import CartCounter from "../CartCounter"
import FavoriteCounter from "../FavoriteCounter"
import Sidebar from "../Sidebar"

const Header = () => {
    return (
        <div className="header">
            <div className="header__block">
                <Sidebar />
            </div>
            <div className="header__block">
                <CartCounter />
                <FavoriteCounter />
            </div>
        </div>
    )
}

export default Header
