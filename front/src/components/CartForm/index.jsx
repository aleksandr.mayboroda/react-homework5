import React from "react"
import { Formik, Form, Field } from "formik"
import * as yup from "yup"

import "./style.scss"

import { useDispatch, useSelector } from "react-redux"

import { modalOperations } from "../../store/modal"
import { cartOperations, cartSelectors } from "../../store/cart"
import { userOperations, userSelectors } from "../../store/user"

import FormField from "../FormField"
import NumberFormat from "react-number-format"

//универсальная ошибка, чтобы не дублировать
const isRequiredError = "This field is required"

const userFormSchema = yup.object().shape({
    firstName: yup
        .string()
        .required(isRequiredError)
        .min(4, "Enter min 4 chars"),
    lastName: yup
        .string()
        .required(isRequiredError)
        .min(4, "Enter min 4 chars"),
    age: yup
        .number("Must be a number")
        .required(isRequiredError)
        .min(18, "You mast be over 18 eyars")
        .max(99, "I dont't think you older then 99"),
    address: yup
        .string()
        .required(isRequiredError)
        .min(
            5,
            "Your address must be correct to get your order - 5 chars minimum"
        ),
    // phone: yup
    //     .string()
    //     .matches(
    //         /\+380[\d]{2}[\d]{7}/,
    //         "Phone need to be in format +380xxXXXxxXX"
    //     ),
    phone: yup.string().required(isRequiredError).matches(/\+38\([\d]{3}\)\s[\d]{3}-[\d]{2}-[\d]{2}/,'Phone need to be in format +38(0xx) XXX-XX-XX'),
    email: yup.string().email("Enter correct email"),
})

const CartForm = () => {
    const userData = useSelector(userSelectors.getUserData())
    const productsOnCart = useSelector(cartSelectors.getCart())
    // console.log("userData", userData)

    const dispatch = useDispatch()

    const handleSubmit = (fieldValues) => {
        // console.log("Submit", fieldValues)
        //1. Сохранить пользователя в localStorage
        //2. Сохранить заказ в localStorage
        //3. Удалить список заказанных товаров пользователя из localStorage cart
        //4. Закрыть окно

        //1.
        dispatch(userOperations.setUser(fieldValues))

        //2.
        dispatch(userOperations.setOrder(productsOnCart))

        //3.
        dispatch(cartOperations.clearCart())

        //4.
        dispatch(modalOperations.setModalContent(null))

        console.log("you have bought:", productsOnCart)
    }

    return (
        <Formik
            initialValues={{
                firstName: userData.firstName || "",
                lastName: userData.lastName || "",
                age: userData.age || "",
                address: userData.address || "",
                phone: userData.phone || "",
                email: userData.email || "",
            }}
            validationSchema={userFormSchema}
            onSubmit={handleSubmit}
        >
            {(formikProps) => (
                <Form noValidate className="cart-form form">
                    {/* {console.log("formikProps", formikProps)} */}

                    <Field
                        type="text"
                        placeholder="First Name"
                        label="first name"
                        name="firstName"
                        className="form__element"
                        component={FormField}
                    />
                    <Field
                        type="text"
                        placeholder="Last Name"
                        label="last name"
                        name="lastName"
                        className="form__element"
                        component={FormField}
                    />
                    <Field
                        type="text"
                        placeholder="Age"
                        label="age"
                        name="age"
                        className="form__element"
                        component={FormField}
                    />
                    <Field
                        type="text"
                        placeholder="Delivery Address"
                        label="delivery address"
                        name="address"
                        className="form__element"
                        tag="textarea"
                        rows="3"
                        component={FormField}
                    />
                    {/* <Field
                        type="phone"
                        placeholder="+380xxXXXxxXX"
                        label="phone number"
                        name="phone"
                        className="form__element"
                        component={FormField}
                    /> */}

                    <div
                        className={`form__element ${
                            formikProps.touched["phone"] &&
                            formikProps.errors["phone"] &&
                            "form__element-error"
                        }`}
                    >
                        <label htmlFor="phone">phone number</label>
                        <NumberFormat
                            format="+38(###) ###-##-##"
                            placeholder="+38(###) ###-##-##"
                            name="phone"
                            type="phone"
                            defaultValue={userData.phone || ''}
                            onBlur={formikProps.handleBlur}
                            onChange={formikProps.handleChange}
                        />
                        {formikProps.touched["phone"] &&
                            formikProps.errors["phone"] && (
                                <p className="form__error">
                                    {formikProps.errors["phone"]}
                                </p>
                            )}
                    </div>

                    <Field
                        type="email"
                        placeholder="Email"
                        label="email"
                        name="email"
                        className="form__element"
                        component={FormField}
                    />
                    <div className="form__element">
                        <button
                            type="submit"
                            className="btn form__submit-btn btn-success"
                            disabled={
                                Object.keys(formikProps.errors).length !== 0
                            }
                        >
                            Submit
                        </button>
                    </div>
                </Form>
            )}
        </Formik>
    )
}

export default CartForm
