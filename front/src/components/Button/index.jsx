import "./style.scss";
import PropTypes from "prop-types";

const Button = ({ text, func, className = "", active=true }) => 
{
  return (
    <button className={`btn ${className}`} onClick={func} disabled={!active}>
      {text}
    </button>
  );
}

Button.propTypes = {
  text: PropTypes.string.isRequired,
  func: PropTypes.func.isRequired,
  className: PropTypes.string,
};

Button.defaultProps = {
  className: "",
};

export default Button;
